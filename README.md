# Jaeger install on Ubuntu 16
## Links
* [Components](https://www.jaegertracing.io/docs/1.8/architecture/#components)
* [Deployment](https://www.jaegertracing.io/docs/1.8/deployment/)
* [Releases](https://github.com/jaegertracing/jaeger/releases)

## Use
### Ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash
allow_world_readable_tmpfiles=True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### Hosts example
```bash
cat <<'EOF' > ./hosts
[install_jaeger_collector]
jaeger-host ansible_host=192.168.1.15

EOF
```

### Download ubuntu role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/jaeger.git
  name: jaeger
EOF
echo "roles/jaeger" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

### Jaeger collector and query install
```bash
cat <<'EOF' > install_jaeger_collector.yml
---
- name: Install jaeger-collector
  hosts:
    install_jaeger_collector
  become: yes
  become_user: root
#  environment:
#    http_proxy: http://squid.ru:3128
#    https_proxy: http://squid.ru:3128
  tasks:
    - name: Jaeger-collector install
      include_role:
        name: jaeger
      vars:
        jaeger_action: 'install_collector'
        jaeger_es_server_url: 'http://192.168.12.114:9200'
        jaeger_download: true

    - name: Jaeger-query install
      include_role:
        name: jaeger
      vars:
        jaeger_action: 'install_query'
        jaeger_es_server_url: 'http://192.168.12.114:9200'
        jaeger_download: false

    - name: Jaeger-agent install
      include_role:
        name: jaeger
      vars:
        jaeger_action: 'install_agent'
        jaeger_collector_host_port: '192.168.12.116:14267'
        jaeger_download: false

EOF

ansible-playbook install_jaeger_collector.yml
```

## Trash
### jaeger user
```bash
useradd -c 'jaeger service' -r -m -U -d /opt/jaeger jaeger
```
### jaeger-collector
```bash
cat <<'EOF' > /lib/systemd/system/jaeger-collector.service
[Unit]
Description=Jaeger-collector
After=local-fs.target network.target

[Service]
Type=forking
ExecStart=/bin/bash /opt/jaeger/jaeger-collector.bash
User=jaeger
Restart=always
RestartSec=10

[Install]
WantedBy=default.target
EOF

cat <<'EOF' > /opt/jaeger/jaeger-collector.bash
#!/bin/bash
SPAN_STORAGE_TYPE=elasticsearch /opt/jaeger/jaeger-collector \
 --es.server-urls http://elastic:9200 &
EOF

chmod +x /opt/jaeger/jaeger-collector.bash
systemctl daemon-reload
systemctl enable jaeger-collector
systemctl start jaeger-collector
systemctl status jaeger-collector
```
